/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : udp_types.c                                                   */
/* Author   : Laughlin Barker                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2019                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#include "udp_types.h"

uint8_t checksum8(const void *ptr, uint16_t nbytes)
{
	const uint8_t *localPtr = ptr;
	uint16_t sum;
	for ( sum = 0 ; nbytes != 0 ; nbytes-- )
		sum = (sum + *(uint8_t *)(localPtr++) ) % 256;
	return (uint8_t)sum;
}

int is_pack_on(PACK_DATA_T *pdata)
{
	return ( (pdata->aux_data_bits & (STATE_MASK)) == ON);
}

int num_29packs(BALL_DATA_892_T *bdata)
{
	int n = 0;
	int i;
	for (i = 0; i < NPACKS; i++)
	{
		int type_num = (bdata->pack[i].aux_data_bits & (TYPE29 << TYPE_LSB)) >> TYPE_LSB;
		if (type_num == TYPE29)
		{
			n++;
		}
	}
	return n;
}

int num_34packs(BALL_DATA_892_T *bdata)
{
	int n = 0;
	int i;
	for (i = 0; i < NPACKS; i++)
	{
		int type_num = (bdata->pack[i].aux_data_bits & (TYPE34 << TYPE_LSB)) >> TYPE_LSB;
		if (type_num == TYPE34)
		{
			n++;
		}
	}
	return n;

}
