/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : udp_types.c                                                   */
/* Author   : Laughlin Barker                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2019                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#ifndef BATMAN_UDP_TYPES_H_
#define BATMAN_UDP_TYPES_H_

#if defined(_QNX)
	#include <sys/types.h>
#else
	#include <stdint.h>
#endif

#define STATE_LSB 0
#define TYPE_LSB 2
#define STATE_MASK 3
#define TYPE_MAS 3

#define CHG_LSB 0
#define DISCH_LSB 1
#define AUX_LSB 2
#define AUX2_LSB 3

#define NPACKS 55

//Battery State Aux Bits
enum AuxStateBits {
	OFF,
	ON,
	SHUTDOWN,
	NO_COMMS
};

//Battery Type Aux Bits
enum AuxTypeBits {
	UNKNOWN,
	TYPE29,
	TYPE34
};


typedef struct PACK_CELL_VOLTS_S {
	uint16_t cell_mV[8];
} PACK_CELL_VOLTS_T; //16 bytes, aligns cleanly on 32-bit system
/**
 * @brief Data structure for holding individual battery pack statistics.
 * 
 */
typedef struct PACK_DATA_S{
	uint16_t ser_num;
	uint16_t cycle_cnt;
	uint16_t volts_mV;
	int16_t current_mA;
	int16_t temp_Cx10;
	uint16_t status;
	uint16_t aux_data_bits;
	uint16_t remaining_capacity_mAh;
} PACK_DATA_T; //16 bytes, aligns cleanly with 4-byte words on our 32-bit system

/*
 *             aux_data_bits layout
 * 0-1: STATE
 * 2-3: BATTERY TYPE
 * 4-15: UNUSED
 *
 * STATE:	0-OFF, 1-ON, 2-SHUTDOWN, 3-NO COMMS
 * TYPE:	0-UNKNOWN, 1-TYPE_29, 2-TYPE_34
 *
 * For example, to get battery type: uint8_t state = (data->pack[batt].aux_data_bits & (0b11 << STATE_LSB)) >> STATE_LSB;
 * Note that 0b is a GCC extension
*/

/**
 * @brief DEPRICATED
 * 
 */
typedef struct BALL_DATA_S{
	PACK_DATA_T pack[NPACKS];		//12*55 = 660 bytes
	uint16_t status_bits;	//660+2 = 662 bytes (need 2 bytes for clean alignment)
	uint8_t relhum_pct;		//662+1 = 663 bytes (need 1 byte)
	uint8_t padding;		//663+1 = 664 bytes. 664%4=0 ==> Word aligned for 32-bit systems
} BALL_DATA_T;

/**
 * @brief Ball data message type (672 bytes) containing individual pack data, and battery subsystem data.
 * 
 */
typedef struct BALL_DATA_892_S{
	PACK_DATA_T pack[NPACKS];		//16*55 = 880 bytes. 880%4=0
	uint16_t status_bits;		//882
	int16_t aux_curr_Ax10;		//884
	int16_t disch_curr_Ax10;	//886
	int16_t chrg_curr_Ax10;		//888
	int16_t fet_temp_Cx10;		//890
	uint8_t relhum_pct;			//891
	uint8_t chksum;		//891+1 = 892 bytes. 892%4=0 ==> Word aligned for 32-bit system
} BALL_DATA_892_T;

typedef struct BALL_DATA_884_S {
	PACK_CELL_VOLTS_T pack[NPACKS];		//16*55 = 880 bytes. 880%4 = 0
	uint8_t padding[3];					//883
	uint8_t chksum;						//884
} BALL_DATA_884_T;

/**
 * @brief Notification message type from battery ball indicating impending shutdown
 * 
 */
typedef struct SHUTDOWN_WARN_S{
	uint8_t sec_to_shutdown;
	uint8_t num_packs_remaining;
	uint8_t padding;
	uint8_t chksum;
} SHUTDOWN_WARN_T;

uint8_t checksum8 (const void *ptr, uint16_t nbytes);

int is_pack_on(PACK_DATA_T *pdata);
int num_29packs(BALL_DATA_892_T *bdata);
int num_34packs(BALL_DATA_892_T *bdata);

/*
 *             status_bits layout
 * 0: CHARGE ENABLE
 * 1: DISCHARGE ENABLE
 * 2: AUX
 * 3: AUX2
 * 4-15: UNUSED
 *
*/
#endif
