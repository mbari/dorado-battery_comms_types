# battery_comms_types

This repository contains definitions of binary data structures used to transmit battery status infromation from the 55-pack Inspired Energy based battery packs used in the Dorado AUVs.

This repository should be added as a Git Submodule to the project which requires it.

To add this project as a Submodule, do the following:
```
$ cd auv_project/src/include_dir
$ git submodule add git@bitbucket.org:mbari/battery_comms_types.git
Cloning into 'batman/src/battery_comms_types'...
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
```

This will add a .gitmodules file, and the folder, battery_comms_types; use `git status` to see this:
```
$ git status
On branch udp_transition
Your branch is up to date with 'origin/udp_transition'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   ../.gitmodules
	new file:   battery_comms_types

Untracked files:
  (use "git add <file>..." to include in what will be committed)
```

For additional details on Git Submodules, see [Git - Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
